import { createGlobalStyle } from "styled-components";
import DeezerProductMedium from "./assets/fonts/deezer-product-medium.woff2";
import DeezerProductBold from "./assets/fonts/deezer-product-bold.woff2";
import DeezerProductExtraBold from "./assets/fonts/deezer-product-extrabold.woff2";

const FontStyles = createGlobalStyle`
@font-face {
  font-family: 'Deezer Product';
  src: url(${DeezerProductMedium}) format('woff2'),
    url(${DeezerProductBold}) format('woff2'),
    url(${DeezerProductExtraBold}) format('woff2');
}

body {
  font-family: 'Deezer Product', sans-serif;
}
`;

export default FontStyles;