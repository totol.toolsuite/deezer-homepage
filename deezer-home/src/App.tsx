import './App.css'
import LiveTheMusicBlock from './components/LiveTheMusicBlock/LiveTheMusicBlock'
import Navbar from './components/Navbar/Navbar'
import { createGlobalStyle } from "styled-components";
import DeezerProductMedium from "./assets/fonts/deezer-product-medium.woff2";
import DeezerProductBold from "./assets/fonts/deezer-product-bold.woff2";
import DeezerProductExtraBold from "./assets/fonts/deezer-product-extrabold.woff2";
import CardsBlock from './components/CardsBlock/CardsBlock';

export const FontStyles = createGlobalStyle`
@font-face {
  font-family: 'Deezer Product';
  src: url(${DeezerProductMedium}) format('woff2'),
    url(${DeezerProductBold}) format('woff2'),
    url(${DeezerProductExtraBold}) format('woff2');
}

body {
  font-family: 'Deezer Product', sans-serif;
}
`;

function App() {

  return (
    <>
      <Navbar></Navbar>
      <LiveTheMusicBlock></LiveTheMusicBlock>
      <CardsBlock></CardsBlock>
    </>
  )
}

export default App
