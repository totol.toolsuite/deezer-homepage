import styled, { keyframes } from "styled-components";
import Button from "../Button/Button";

export const CardsBlockContainer = styled.div`
  position: relative;
`

export const CardsBlockBGContainer = styled.div`
  position: absolute;
  top: -50px;
  height: 600px;
  width: 100vw;
  background-color: var(--purple-bg);
  clip-path: ellipse(90% 49% at 50% 48%);
`

export const CardsContainer = styled.div`
  z-index: 50;
  display: flex;
  flex-direction: column;
  gap: 22px;
  align-items: center;
  justify-content: center;
  transform: translateY(-140px);
`

export const CardContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 8px;
`

export const Tag = styled.span<{$color: 'black' | 'purple' | 'red'}>`
  font-size: 1.6rem;
  font-weight: 700;
  padding: 2px 10px 10px 10px;
  background-color: ${({$color}) => $color === 'black' ? 'var(--dark-bg)' : $color === 'purple' ? 'var(--purple-bg)' : 'var(--red-bg)'};
  color: #FFFFFF;
  clip-path: ellipse(70% 60% at 50% 50%);
  border-radius: 12px;
`

export const FullWidthButton = styled(Button)`
  align-self: stretch;
  text-align: center;
  padding: 16px 0px;
`

export const CardSubtitle = styled.div`
  font-size: 18px;
  font-weight: 900;
`