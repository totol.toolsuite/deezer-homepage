import Button from "../Button/Button";
import Card from "../Card/Card";
import { CardsBlockBGContainer, CardsContainer, CardsBlockContainer, Tag, CardContent, FullWidthButton, CardSubtitle } from "./CardsBlock.elements";

const CardsBlock = () => {
  return (
    <CardsBlockContainer>
      <CardsBlockBGContainer>
      </CardsBlockBGContainer>
      <CardsContainer>
        <Card>
          <CardContent>
            <Tag $color="black">DEEZER GRATUIT</Tag>
            <div>Musique, podcasts, radio et recommandations personnalisées.</div>
            <FullWidthButton type="secondary">S'inscrire gratuitement</FullWidthButton>
            <div>
              <div>Aucun paiement nécessaire</div>
              <a href="#">En savoir plus</a>
            </div>
          </CardContent>
        </Card>
        <Card>
          <CardContent>
            <Tag $color="purple">DEEZER GRATUIT</Tag>
            <CardSubtitle>2 MOIS OFFERTS</CardSubtitle>
            <div>Écoute les titres de ton choix, sans aucune publicité, et télécharge tes favoris pour les écouter hors connexion. Profite du son Haute Fidélité sur tous tes appareils.</div>
            <FullWidthButton type="secondary">Essayer gratuitement</FullWidthButton>
            <div>
              <div>puis 11,99 €/mois.</div>
              <div>Sans engagement, résilie à tout moment.</div>
              <a href="#">En savoir plus</a>
            </div>
          </CardContent>
        </Card>
      </CardsContainer>
    </CardsBlockContainer>
  )
}
export default CardsBlock;