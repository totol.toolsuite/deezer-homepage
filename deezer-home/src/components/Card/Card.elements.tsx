import styled from "styled-components";

export const WhiteCard = styled.a`
  width: 100%;
  max-width: 600px;
  padding: 20px;
  background-color: #FFFFFF;
  border-radius: 6px;
`
