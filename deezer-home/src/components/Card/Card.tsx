import { ReactElement } from "react";
import { WhiteCard } from "./Card.elements";

const Card = ({ children }: { children: string | ReactElement }) => {
  return <WhiteCard>
    {children}
  </WhiteCard>
}
export default Card;