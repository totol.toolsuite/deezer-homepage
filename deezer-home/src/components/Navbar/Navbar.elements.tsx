import styled from "styled-components"

export const Nav = styled.nav`
  background-color: var(--dark-bg);
  color: #FFFFFF;
  height: 110px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 0 4rem;
  border-bottom: 1px solid var(--border-neutral-color)
`

export const ActionsContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 55px;
  align-items: center;
`

export const LinksContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 21px;
`

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 12px;
`

export const Link = styled.a`
  font-size: 1.25rem;
  font-weight: 700;
  text-decoration: none;
  color: inherit;

  &:hover {
    color: var(--purple-bg);
  }
`

export const DeezerLogo = styled.img`
  height: 35%;
`