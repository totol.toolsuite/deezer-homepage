import Button from "../Button/Button";
import { ActionsContainer, ButtonsContainer, DeezerLogo, Link, LinksContainer, Nav } from "./Navbar.elements";

const Navbar = () => {
  return <Nav>
    <DeezerLogo src="https://e-cdn-files.dzcdn.net/cache/slash/images/common/logos/logo-horizontal-white-text.c409af08ede4db772948.svg"></DeezerLogo>
    <ActionsContainer>
      <LinksContainer>
        <Link href="#">Offres</Link>
        <Link href="#">Fonctionnalités</Link>
        <Link href="#">Musique</Link>
      </LinksContainer>
      <ButtonsContainer>
        <Button href="#" type="secondary">Connexion</Button>
        <Button href="#" >Inscription</Button>
      </ButtonsContainer>
    </ActionsContainer>
  </Nav>
}
export default Navbar;