import styled from "styled-components";

export const Btn = styled.a<{$type: 'primary' | 'secondary'}>`
  font-size: 1rem;
  padding: .5em 1.15em;
  background-color: ${props => props.$type === 'primary' ? 'var(--purple-bg)' : 'var(--dark-bg)'};
  color: white;
  border: none;
  border-radius: 10px;
  font-weight: 700;
  border: ${props => props.$type === 'primary' ? 'none' : '.5px solid var(--border-neutral-color)'};
  text-decoration: none;
  color: 'inherit';
  transition: background-color 0.5s ease;

  &:hover {
    background-color: ${props => props.$type === 'primary' ? 'var(--purple-bg-hover)' : 'var(--dark-bg-hover)'};
  }
`
