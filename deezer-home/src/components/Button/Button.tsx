import { ReactElement } from "react";
import { Btn } from "./Button.elements";

const Button = ({type = 'primary', children, ...props}: {type?: 'primary' | 'secondary', [x:string]: any, children: string | ReactElement}) => {
  return <Btn $type={type} {...props}>
    {children}
  </Btn>
}
export default Button;