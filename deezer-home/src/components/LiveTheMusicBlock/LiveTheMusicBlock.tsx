import { LiveTheMusicContainer, OvalBounce, OvalChain, SliddingText, UppercasedText } from "./LiveTheMusicBlock.elements";

const OvalChainSvg = () => {
  return <svg height={80} width={210}>
  {
  [...Array(12).keys()].map((x, idx) => (
    <OvalChain height="80" width="20" x={`${(idx + 1) * 15}`} rx="50" $speed={4} $delay={idx * 0.1}></OvalChain>
  ))
  }
</svg>
}

const OvalBounceSvg = () => {
  return <svg height={80} width={140}>
  {
    <>
      <OvalBounce height="80" width="35" x="15" rx="50" $speed={.7} $delay={0}></OvalBounce>
      <OvalBounce height="70" width="15" x="47"  rx="50" y="10" $speed={.4} $delay={0.3}></OvalBounce>
      <OvalBounce height="80" width="35" x="61" rx="50" $speed={.6} $delay={0.5}></OvalBounce>
      <OvalBounce height="70" width="15" x="94" rx="50" y="10" $speed={.6} $delay={0.2}></OvalBounce>
      <OvalBounce height="40" width="15" x="107" rx="50" $speed={.4} y="40" $delay={0}></OvalBounce>
    </>
  }
</svg>
}


const LiveTheMusicBlock = () => {
  return <LiveTheMusicContainer>
    <SliddingText $speed={8}>
      <UppercasedText>live the music</UppercasedText>
      <OvalBounceSvg></OvalBounceSvg>
      <UppercasedText>live the music</UppercasedText>
      <OvalChainSvg></OvalChainSvg>
      <UppercasedText>live the music</UppercasedText>
      <OvalChainSvg></OvalChainSvg>
    </SliddingText>
    <SliddingText $speed={13}>
      <UppercasedText>live the music</UppercasedText>
      <OvalChainSvg></OvalChainSvg>
      <UppercasedText>live the music</UppercasedText>
      <OvalBounceSvg></OvalBounceSvg>
      <UppercasedText>live the music</UppercasedText>
      <OvalBounceSvg></OvalBounceSvg>
    </SliddingText>
  </LiveTheMusicContainer>
}
export default LiveTheMusicBlock;