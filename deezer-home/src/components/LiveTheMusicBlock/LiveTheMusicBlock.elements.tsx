import styled, { keyframes } from "styled-components";

const slide = keyframes`
  from {
    transform: translateX(0);
  }

  to {
    transform: translateX(-33%);
  }
`;

const growingSvg = keyframes`
  0% {
    transform: scaleY(0);
  }
  5% {
    transform: scaleY(1);
  }
  40% {
    transform: scaleY(1);
  }
  45% {
    transform: scaleY(0);
  }
  100% {
    transform: scaleY(0);
  }
`;

const bouncingSvg = keyframes`
  0% {
    transform: scaleY(0.75);
  }
  50% {
    transform: scaleY(1);
  }
  100% {
    transform: scaleY(0.75);
  }
`;


export const LiveTheMusicContainer = styled.div`
  background-color: var(--dark-bg);
  height: 520px;
  padding: 20px 0px;
`

export const SliddingText = styled.h1<{$speed: number}>`
  color: #FFFFFF;
  margin: 0;
  display: inline-block;
  max-width: none;
  white-space: nowrap;
  animation: ${slide} ${({$speed}) => $speed}s linear infinite;
  --animation-drop-percentage: 0;
`

export const UppercasedText = styled.span`
  text-transform: uppercase;
  font-size: 10rem;
  font-family: 'Deezer Product', sans-serif;
  font-weight: 700;
  text-rendering: optimizeLegibility;
  line-height: 1;
`

export const OvalChain = styled.rect<{$speed: number, $delay: number}>`
  fill: var(--purple-bg);
  transform-origin: bottom;
  animation: ${growingSvg} ${({$speed}) => $speed}s ease-in-out infinite;
  animation-delay: ${({$delay}) => $delay}s;
`


export const OvalBounce = styled.rect<{$speed: number, $delay: number}>`
  fill: var(--yellow-bg);
  transform-origin: bottom;
  animation: ${bouncingSvg} ${({$speed}) => $speed}s linear infinite;
  // animation-delay: ${({$delay}) => $delay}s;
`